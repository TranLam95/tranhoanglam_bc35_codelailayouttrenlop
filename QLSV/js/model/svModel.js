

function Sinhvien(_ma,_ten,_email,_matKhau,_ngaySinh,_khoaHoc,_diemLy,_diemHoa,_diemToan){
    this.ma = _ma;
    this.ten = _ten;
    this.email = _email;
    this.matKhau = _matKhau;
    this.ngaySinh = _ngaySinh;
    this.khoaHoc = _khoaHoc;
    this.diemLy = _diemLy;
    this.diemHoa = _diemHoa;
    this.diemToan = _diemToan;

    this.tinhDTB=function(){
        var dtb = (this.diemToan*1 + this.diemHoa*1 + this.diemLy*1)/3;
        return dtb.toFixed(1);
    }
}
