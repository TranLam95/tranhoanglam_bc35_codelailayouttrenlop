

const DSSV = "DSSV";

var dssv = [];
var dataJson = localStorage.getItem(DSSV);
if(dataJson)
{
    var dataRaw = JSON.parse(dataJson);
    dssv = dataRaw.map(function(sv){ 
        return new Sinhvien(
            sv.ma,
            sv.ten,
            sv.email,
            sv.matKhau,
            sv.ngaySinh,
            sv.khoaHoc,
            sv.diemLy,
            sv.diemHoa,
            sv.diemToan,
        );
    });
    renderDssv(dssv);
}
function saveLocalStorage()
{
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dssvJson);
    renderDssv(dssv);
    document.getElementById("formQLSV").reset();
}

function themSv()
{
    var newSv = layThongTinTuForm();
    dssv.push(newSv);
    saveLocalStorage();
    renderDssv(dssv);
    resetForm();
}

function xoaSv(idSv)
{
    var index = dssv.findIndex(function(sv){
        return sv.ma == idSv;
    });
    if (index == -1) return;
    dssv.splice(index,1);
    saveLocalStorage();
    renderDssv(dssv);
}

function suaSv(idSv){
    var index = dssv.findIndex(function(sv){
        return sv.ma == idSv;
    });
    if(index == -1) return;

    var sv = dssv[index];
    showThongTinLenForm(sv);
    document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv(idSv){
    var svEdit = layThongTinTuForm();
    var index = dssv.findIndex(function(sv){
        return sv.ma == svEdit.ma;
    });
    if(index == -1) return;
    dssv[index] = svEdit;
    saveLocalStorage();
    renderDssv(dssv);
    resetForm();
    document.getElementById("txtMaSV").disabled = false;
}
