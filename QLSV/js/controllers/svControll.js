function layThongTinTuForm(){
    var maSV = document.getElementById("txtMaSV").value.trim();
    var tenSV = document.getElementById("txtTenSV").value.trim();
    var email = document.getElementById("txtEmail").value.trim();
    var matKhau = document.getElementById("txtPass").value.trim();
    var ngaySinh = document.getElementById("txtNgaySinh").value.trim();
    var khoaHoc = document.getElementById("khSV").value.trim();
    var diemLy = document.getElementById("txtDiemLy").value.trim();
    var diemHoa = document.getElementById("txtDiemHoa").value.trim();
    var diemToan = document.getElementById("txtDiemToan").value.trim();

    var sv = new Sinhvien(maSV,tenSV,email,matKhau,ngaySinh,khoaHoc,diemLy,diemHoa,diemToan)
    return sv;
}
function renderDssv(list)
{
    var contentHTML = "";
    for(var i=0;i<list.length;i++)
    {
        var currentSv = list[i];
        var contentTr=`<tr>
        <td>${currentSv.ma}</td>
        <td>${currentSv.ten}</td>
        <td>${currentSv.email}</td>
        <td>${currentSv.ngaySinh}</td>
        <td>${currentSv.khoaHoc}</td>
        <td>${currentSv.tinhDTB()}</td>
        <td> 
        <button class="btn btn-danger" onclick="xoaSv('${currentSv.ma}')">Xóa</button>
        <button class="btn btn-primary" onclick="suaSv('${currentSv.ma}')">Sửa</button>
        </td>
        </tr>`;
        contentHTML +=contentTr;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showThongTinLenForm(sv){
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtNgaySinh").value = sv.ngaySinh;
    document.getElementById("khSV").value = sv.khoaHoc;
    document.getElementById("txtDiemLy").value = sv.diemLy;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
    document.getElementById("txtDiemToan").value = sv.diemLy;
}

function resetForm(){
    document.getElementById("formQLSV").reset();
}